<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
       
        DB::table('roles')->insert([
            'name' => 'Cliente',
            'guard_name' => 'web'
        ]);

    }
}
