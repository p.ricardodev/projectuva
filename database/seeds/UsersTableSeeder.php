<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $objDateTime = new DateTime('NOW');

        DB::table('users')->insert([
            'name' => 'Administrador',
            'cpf' => 0,
            'telefone' => 0,
            'email' => 'a&t@a&tstore.com',
            'password' => Hash::make('@admin@admin'),
            'created_at' => $objDateTime
        ]);

        DB::table('roles')->insert([
            'name' => 'Administrador',
            'guard_name' => 'web'
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 1
        ]);

    }
}
