<?php

Route::get('/', 'HomeController@index')->name('/');

Auth::routes();

//ACL==================================================================================================
Route::get('user/{user}/roles', 'UserController@roles')->name('user.roles')->middleware(['auth','role:Administrador']);
Route::put('user/{user}/roles', 'UserController@rolesSync')->name('user.roles')->middleware(['auth','role:Administrador']);

Route::resource('user', 'UserController');//->middleware(['auth','role:Administrador']);

Route::get('role/{role}/permissions', 'RoleController@permissions')->name('role.permissions')->middleware(['auth','role:Administrador']);
Route::put('role/{role}/permissions', 'RoleController@permissionsSync')->name('role.permissions')->middleware(['auth','role:Administrador']);
Route::resource('role', 'RoleController')->middleware(['auth','role:Administrador']);

Route::resource('permission', 'PermissionController')->middleware(['auth','role:Administrador']);
//======================================================================================================
Route::resource('produto', 'ProdutoController')->middleware(['auth','role:Administrador']);
Route::resource('categoria', 'CategoriaController')->middleware(['auth','role:Administrador']);

Route::get('/carrinho', 'CarrinhoController@index')->name('carrinho.index')->middleware('auth');
Route::get('/carrinho/adicionar', function() {
    return redirect()->route('/');
});
Route::post('/carrinho/adicionar', 'CarrinhoController@adicionar')->name('carrinho.adicionar')->middleware('auth');
Route::delete('/carrinho/remover', 'CarrinhoController@remover')->name('carrinho.remover')->middleware('auth');
Route::post('/carrinho/concluir', 'CarrinhoController@concluir')->name('carrinho.concluir')->middleware('auth');
Route::get('/carrinho/compras', 'CarrinhoController@compras')->name('carrinho.compras')->middleware('auth');
Route::post('/carrinho/cancelar', 'CarrinhoController@cancelar')->name('carrinho.cancelar')->middleware('auth');
Route::post('/carrinho/desconto', 'CarrinhoController@desconto')->name('carrinho.desconto')->middleware('auth');

/*ROTA DE TESTE FEATURE*/
Route::get('routeClosure', function() {
    return 'OK';
});