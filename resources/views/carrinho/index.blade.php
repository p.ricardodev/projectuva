﻿@extends('layouts.layout')


@section('content')

<div class="container">
    <div class="row">
        <h3>Produtos no carrinho</h3>
        <hr/>
        @if (Session::has('mensagem-sucesso'))
            <div class="card-panel green">
                <strong>{{ Session::get('mensagem-sucesso') }}</strong>
            </div>
        @endif
        @if (Session::has('mensagem-falha'))
            <div class="card-panel red">
                <strong>{{ Session::get('mensagem-falha') }}</strong>
            </div>
        @endif
        @forelse ($pedidos as $pedido)
            <h5 class="col l6 s12 m6"> Pedido: #{{ $pedido->id }} </h5>
            <h5 class="col l6 s12 m6"> Criado em: {{ $pedido->created_at->format('d/m/Y H:i') }} </h5>
            <table class="table table-striped table-hover table-responsive">
                <thead>
                    <tr>
                        <th></th>
                        <th>Qtd</th>
                        <th>Produto</th>
                        <th>Valor Unit.</th>
                      
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total_pedido = 0;
                    @endphp
                    @foreach ($pedido->pedido_produtos as $pedido_produto)

                    <tr>
                        <td>
                            <img width="100" height="100" src="{{ asset('images/'.$pedido_produto->produto->imagem) }}"><br />
                            <a href="#" onclick="carrinhoRemoverProduto({{ $pedido->id }}, {{ $pedido_produto->produto_id }}, 0)" style="color:red;" data-position="right" data-delay="50" data-tooltip="Retirar produto do carrinho?"><i class="fas fa-trash"></i> Retirar produto</a>

                        </td>
                        <td class="center-align">
                            <div class="center-align">
                                <a class="col l4 m4 s4" href="#" onclick="carrinhoRemoverProduto({{ $pedido->id }}, {{ $pedido_produto->produto_id }}, 1 )">
                                    <i class="fas fa-minus"></i>
                                </a>
                                <span class="col l4 m4 s4"> {{ $pedido_produto->qtd }} </span>
                                <a class="col l4 m4 s4" href="#" onclick="carrinhoAdicionarProduto({{ $pedido_produto->produto_id }})">
                                <i class="fas fa-plus"></i>
                                </a>
                            </div>
                        </td>
                        <td> {{ $pedido_produto->produto->name }} </td>
                        <td>R$ {{ number_format($pedido_produto->produto->preco_venda, 2, ',', '.') }}</td>
                       
                        @php
                            $total_produto = $pedido_produto->valores;
                            $total_pedido += $total_produto;

                        @endphp

                        <td>R$ {{ number_format($total_produto, 2, ',', '.') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
            <div class="col-md-4" style="font-size:1.8rem">
                <strong class="col offset-l6 offset-m6 offset-s6 l4 m4 s4 right-align">Total do pedido: </strong>
                <span class="col l2 m2 s2">R$ {{ number_format($total_pedido, 2, ',', '.') }}</span>
                <br />
                <strong class="col offset-l6 offset-m6 offset-s6 l4 m4 s4 right-align">Total do pedido com desconto: </strong>
                <span class="col l2 m2 s2" id="pedidoDesconto"></span>
            
                </div>
                <div class="col-md-4">

                  <a class="btn btn-info" data-position="top" data-delay="50" data-tooltip="Voltar a página inicial para continuar comprando?" href="{{ route('/') }}">Continuar comprando</a>
                </div>
            </div>

            <div class="row">
            <label>Desconto %</label>
            <input type="hidden" id="valorSemDesconto" value="{{ $total_pedido }}" />
            <input type="text" id="desconto" /> 
            </div>

            <div class="row">
                <form method="POST" action="{{ route('carrinho.concluir') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="pedido_id" value="{{ $pedido->id }}">
                    <input type="hidden" id="formDesconto" name="formDesconto" />
                    <br />
                    <button type="submit" class="btn btn-success" data-position="top" data-delay="50" data-tooltip="Adquirir os produtos concluindo a compra?">
                       <i class="fas fa-check"></i> Finalizar Pedido
                    </button>   
                </form>
            </div>
        @empty
            <h5>Não há nenhum pedido no carrinho</h5>
        @endforelse
    </div>
</div>

<form id="form-remover-produto" method="POST" action="{{ route('carrinho.remover') }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <input type="hidden" name="pedido_id">
    <input type="hidden" name="produto_id">
    <input type="hidden" name="item">
</form>
<form id="form-adicionar-produto" method="POST" action="{{ route('carrinho.adicionar') }}">
    {{ csrf_field() }}
    <input type="hidden" name="id">
</form>

    <script type="text/javascript" src="{{ asset('js/carrinho.js') }}"></script>
<br />

@endsection