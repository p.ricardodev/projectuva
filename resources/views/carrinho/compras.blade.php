@extends('layouts.layout')

@section('content')

<div class="container">
    <div class="row">
        <hr>
        <h4><i class="fas fa-cart-arrow-down"></i> <b>Minhas compras</b></h4> <br />
        @if (Session::has('mensagem-sucesso'))
        <div class="alert alert-success" role="alert">{{ Session::get('mensagem-sucesso') }}</div>
        @endif
        @if (Session::has('mensagem-falha'))
        <div class="alert alert-success" role="danger">{{ Session::get('mensagem-falha') }}</div>
        @endif
        <div class="divider"></div>
        <div class="row col s12 m12 l12">
           
            @forelse ($compras as $pedido)
                <h5 class="col l6 s12 m6"> Pedido: {{ $pedido->id }} </h5>
                <h5 class="col l6 s12 m6"> Criado em: {{ $pedido->created_at->format('d/m/Y H:i') }} </h5>
                <form method="POST" action="{{ route('carrinho.cancelar') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="pedido_id" value="{{ $pedido->id }}">
                    <table class="table table-hover table-striped table-responsive">
                        <thead>
                            <tr>
                                <th colspan="1"></th>
                                <th>Produto</th>
                                <th>Valor</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $total_pedido = 0;
                        @endphp
                        @foreach ($pedido->pedido_produtos_itens as $pedido_produto)
                            @php
                                $total_produto = $pedido_produto->valor;
                                $total_pedido += $total_produto;
                            @endphp

                            @forelse($pedido_produto->descontos as $desconto)
                             @php
                                $valor_desconto = $desconto->valor_desconto;
                                $totalFinalPedido = $total_pedido - $valor_desconto;
                            @endphp
                            @empty
                                @php
                                     $valor_desconto = 0;
                                     $totalFinalPedido = $total_pedido;
                                @endphp
                            @endforelse

                            <tr>
                               
                                <td>
                                    <img width="100" height="100" src="{{ asset('images/'.$pedido_produto->produto->imagem) }}">
                                </td>

                                <td>{{ $pedido_produto->produto->name }}</td>

                                <td>R$ {{ number_format($pedido_produto->valor, 2, ',', '.') }}</td>
                                

                                <td>R$ {{ number_format($total_produto, 2, ',', '.') }}</td>
                                
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4">
                                <strong>Total do pedido</strong>
                                        R$ {{ number_format($total_pedido, 2, ',', '.') }}<br>

                                        <strong>Total do Desconto</strong>
                                        R$ @if($valor_desconto) {{ $valor_desconto }} @else 0 @endif<br>

                                    <strong>Valor a pagar</strong>
                                        R$ {{ number_format($totalFinalPedido, 2, ',', '.') }}
                            </td>
                        </tr>
                            
                        </tbody>
                       
                         
                    </table>
                    <hr>
                    <hr>
                </form>
            @empty
                <h5 class="center">
                    @if ($cancelados->count() > 0)
                        Neste momento não há nenhuma compra valida.
                    @else
                        Você ainda não fez nenhuma compra.
                    @endif
                </h5>
            @endforelse
        </div>
       
       </div>

</div>

@endsection