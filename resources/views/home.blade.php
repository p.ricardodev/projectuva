@extends('layouts.layout')

@section('content')
<aside id="colorlib-hero">
			<div class="flexslider">
				<ul class="slides">
			   	<li style="background-image: url(images/img_bg_1.jpg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner">
				   					<div class="desc">
					   					<h1 class="head-1">Coleção</h1>
					   					<h2 class="head-2">Jeans</h2>
					   					<h2 class="head-3">Masculina</h2>
					   					
				   					</div>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/img_bg_2.jpg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner">
				   					<div class="desc">
									   <h1 class="head-1">Coleção</h1>
					   					<h2 class="head-2">Acessórios</h2>
					   					<h2 class="head-3">Feminino</h2>
				   					</div>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/img_bg_3.jpg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner">
				   					<div class="desc">
					   					<h1 class="head-1">Conheça nossos produtos!</h1>
					   					<h2 class="head-2">Coleção nova mensalmente!</h2>
					   					<h2 class="head-3">Descontos em até 30%</h2>
					   					
				   					</div>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>
		
		<div id="colorlib-featured-product">
			<div class="container">
			<div class="row">
				@forelse($lancamentos as $produto)
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url({{asset('images/'.$produto->imagem)}});">
								<p class="tag"><span class="new">Ainda em Estoque</span></p>
								<div class="cart">
								<form method="POST" action="{{ route('carrinho.adicionar') }}">
										@csrf	
									<input type="hidden" name="id" value="{{ $produto->id }}">
								
										<button class="btn btn-link" data-position="bottom" data-delay="50" data-tooltip="O produto será adicionado ao seu carrinho">
											<i class="fas fa-cart-plus" style="color:white"></i>
										</button>   
										
									</form>
				
								</div>
							</div>
							<div class="desc">
								<h3><a href="">{{ $produto->name }}</a></h3>
								<p class="price"><span>R$ {{ $produto->preco_venda }}</span></p>				
							</div>
						</div>
					</div>
					@empty
		<p> Nenhum produto para ser listado! </p>
		@endforelse
					
				</div>
			</div>
		</div>
		

		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Nova chegada</span></h2>
						<p>Roupas importadas!</p>
					</div>
				</div>
				<div class="row">
				@forelse($importados as $produto)
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url({{asset('images/'.$produto->imagem)}});">
								<p class="tag"><span class="new">Nova coleção</span></p>
								<div class="cart">
								<form method="POST" action="{{ route('carrinho.adicionar') }}">
										@csrf	
									<input type="hidden" name="id" value="{{ $produto->id }}">
								
										<button class="btn btn-link" data-position="bottom" data-delay="50" data-tooltip="O produto será adicionado ao seu carrinho">
											<i class="fas fa-cart-plus" style="color:white"></i>
										</button>   
										
									</form>
				
								</div>
							</div>
							<div class="desc">
								<h3><a href="">{{ $produto->name }}</a></h3>
								<p class="price"><span>R$ {{ $produto->preco_venda }}</span></p>				
							</div>
						</div>
					</div>
					@empty
		<p> Nenhum produto para ser listado! </p>
		@endforelse
					
				</div>
			</div>
		</div>


		<div id="colorlib-intro" class="colorlib-intro" style="background-image: url(images/cover-img-1.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="intro-desc">
							<div class="text-salebox">
								<div class="text-lefts">
									<div class="sale-box">
										<div class="sale-box-top">
											<h3 class="number">Promoção em Breve!</h3>
										</div>
										</div>
											
								</div>
								<div class="text-rights">
									<h3 class="title">Fica ligado no site!</h3>
									<p>Estamos preparando as melhores ofertas exclusivamente para você nosso cliente!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Outras tendências</span></h2>
						<p>Confira nosso mais variados produtos!</p>
					</div>
				</div>
				<div class="row">
				@forelse($produtos as $produto)			
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url({{asset('images/'.$produto->imagem)}});">
								<div class="cart">
									
									<form method="POST" action="{{ route('carrinho.adicionar') }}">
										@csrf	
									<input type="hidden" name="id" value="{{ $produto->id }}">
								
										<button class="btn btn-link" data-position="bottom" data-delay="50" data-tooltip="O produto será adicionado ao seu carrinho">
											<i class="fas fa-cart-plus" style="color:white"></i>
										</button>   
										
									</form>
				
		
								</div>
							</div>
							<div class="desc">
								<h3><a href="">{{ $produto->name }}</a></h3>
								<p class="price"><span>R$ {{ $produto->preco_venda }}</span></p>
								
							</div>
						</div>
					</div>
					@empty
		<p> Nenhum produto para ser listado! </p>
		@endforelse
				</div>
				{{ $produtos->links("pagination::bootstrap-4") }}
			</div>
		
		</div>
		
@endsection
