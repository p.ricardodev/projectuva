@extends('layouts.layout')

@section('content')
    <div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">EDITAR PERMISSÕES</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('permission.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <form action="{{ route('permission.update', ['permission' => $permission->id]) }}" method="post" class="mt-4" autocomplete="off">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="name">Nome do Perfil</label>
                                <input type="text" class="form-control" id="name" placeholder="Papéis que usuários terão no sistema"
                                       name="name" value="{{ old('name') ?? $permission->name }}">
                            </div>
                            <button type="submit" class="btn btn-success">Alterar</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection
