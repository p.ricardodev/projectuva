@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">LISTAGEM DE PERMISSÕES</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('permission.create') }}">&plus; Novo</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" permission="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" permission="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <table class="table table-striped mt-4">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Permissões</th>
                                  <th class="th-text">Editar</th>
                                 <th class="th-text">Deletar</th>
                            </tr>
                            </thead>
                            <tbody>

                           @foreach($permissions as $permission)
                                <tr>
                                    <td>{{ $permission->id }}</td>
                                    <td>{{ $permission->name }}</td>
                                    <td class="th-text">
                                        <a class="btn btn-link" href="{{ route('permission.edit', ['permission' => $permission->id] ) }}">Editar</a></td>
                                    <td class="th-text">     
                                        <form action="{{ route('permission.destroy', ['permission' => $permission->id]) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <input class="btn btn-link" type="submit" value="Remover">
                                        </form>
                                    </td>
                                </tr>
                          @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><br />
@endsection
