@extends('layouts.layout')

@section('content')
<div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">EDITAR CATEGORIA</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('categoria.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">
                        <form action="{{ route('categoria.update', ['categorium' => $categorias->id]) }}" method="post" class="mt-4" autocomplete="off">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Nome da Categoria</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="name"
                                       name="name" value="{{ old('name') ?? $categorias->name }}">
                                @if ($errors->has('name'))<span class="form-text help-block msg_validate">{{ $errors->first('name') }}</span>@endif

                            </div>
                            
                            <button type="submit" class="btn btn-success">Alterar</button>
                             <a href="{{ route('categoria.index') }}" class="btn btn-info"><i class="fas fa-list"></i> Listar</a>
                            <a href="{{ route('/') }}" class="btn btn-danger"><i class="fas fa-ban"></i> Cancelar</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection