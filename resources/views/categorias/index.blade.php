@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">LISTAGEM DE CATEGORIAS</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('categoria.create') }}">&plus; Novo</a></div>
                        </div>
                    </div><br />
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <table class="table table-striped mt-4">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th class="th-center">Editar</th>
                                <th class="th-center">Excluir</th>
                            </tr>
                            </thead>
                            <tbody>

                           @forelse($categorias as $categoria)
                           <tr>
                                    <td>{{ $categoria->id }}</td>
                                    <td>{{ $categoria->name }}</td>
                                    <td class="th-center"><a class="btn btn-link" href="{{ route('categoria.edit', ['categorium' => $categoria->id] ) }}">Editar</a></td>
                                    <td class="th-center">
                                        <form action="{{ route('categoria.destroy', ['categorium' => $categoria->id]) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <input class="btn btn-link" type="submit" value="Remover">
                                        </form>
                                    </td>
                                </tr>
                                @empty
                            <p>Nenhuma categoria cadastrado!</p>
                          @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><br /><br />
@endsection
