<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>A&T Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">

    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">
    <!-- Flaticons  -->
    <link rel="stylesheet" href="{{ asset('fonts/flaticon/font/flaticon.css') }}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Modernizr JS -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="{{ asset('js/respond.min.js') }}"></script>
	<![endif]-->

</head>

<body>

    @include('modais.modalLogin')

    <div class="colorlib-loader"></div>

    <div id="page">
        <nav class="colorlib-nav" role="navigation">
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-2">
                            <div id="colorlib-logo"><a href="{{ route('/') }}">A&T Store</a></div>
                        </div>
                        <div class="col-xs-10 text-right menu-1">
                            <ul>
                                <li class="active"><a href="{{ route('/') }}">Home</a></li>
                                <li><a href="#sobre">Sobre</a></li>
                                <li><a href="#contato">Contato</a></li>

                                @role('Administrador')
                                <li class="has-dropdown">
                                    <a href="#">Painel Administrativo <i class="fas fa-caret-down"></i></a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('categoria.create') }}">Cadastrar Categoria</a></li>

                                        <li><a href="{{ route('produto.create') }}">Cadastrar Produto</a></li>
                                    </ul>
                                </li>
                                <li class="has-dropdown">
                                    <a href="#">Gestão de Usuários <i class="fas fa-caret-down"></i></a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('user.index') }}">Gestão de usuários</a></li>
                                        <li><a href="{{ route('role.index') }}">Gestão de Perfis</a></li>
                                        <li><a href="{{ route('permission.index') }}">Gestão de Permissões</a></li>
                                    </ul>
                                </li>
                                @endrole

                                <li><a href="{{ route('carrinho.index') }}"><i class="icon-shopping-cart"></i> Carrinho
                                        {{ $qtdpedidos ?? '0'}}</a></li>
                                @if(Auth::check())
                                <li class="has-dropdown">
                                    <a href="#"><b>{{ Auth::user()->name }}</b> <i class="fas fa-caret-down"></i></a>
                                    <ul class="dropdown">
									<li><a href="{{ route('carrinho.compras') }}"><i class="fas fa-cart-arrow-down"></i> Compras</a></i>                                   
                                        <li><a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                <i class="fas fa-sign-out-alt"></i> Sair
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                 
                                    </ul>
                                </li>
                                @else
                                <li><a href="#" data-toggle="modal" data-target="#modalLogin">Login</a></li>
                                <li><a href="{{ route('register') }}">Registrar-se</a></li>
                                @endif
                                <li><a href="http://localhost/frontApi/" target="__blank"><i class="fas fa-check"></i> Vendas</a></i>
                                      
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        @yield('content')

        <footer id="colorlib-footer" role="contentinfo" style="background-color: #DCDCDC">
            <div class="container">
                <div class="row row-pb-md">
                    <div class="col-md-3 colorlib-widget">
                        <h4>Sobre A&T Store</h4>
                        <p>Empresa no ramo de varejo de roupas de diversas marcas, atuando há mais de 10 anos no mercado
                            oferecendo
                            o que há de melhor no seguimento de feminina e masculina</p>
                        <p>
                        <ul class="colorlib-social-icons">
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-linkedin"></i></a></li>
                            <li><a href="#"><i class="icon-instagram"></i></a></li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-md-2 colorlib-widget">
                        <h4>Atendimento ao Consumidor</h4>
                        <p>
                        <ul class="colorlib-footer-links">
                            <li><a href="#">Devolução/Troca</a></li>
                            <li><a href="#">Voucher de presentes</a></li>
                            <li><a href="#">Atendimento ao cliente</a></li>
                            <li><a href="#">Mapa do site</a></li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-md-2 colorlib-widget" id="sobre">
                        <h4>Information</h4>
                        <p>
                        <ul class="colorlib-footer-links">
                            <li><a href="#">Sobre</a></li>
                            <li><a href="#">Informações de Entrega</a></li>
                            <li><a href="#">Políticas de Privacidade</a></li>
                            <li><a href="#">Suporte</a></li>
                            <li><a href="#">Rastreamento do Pedido</a></li>
                        </ul>
                        </p>
                    </div>

                    <div class="col-md-3" id="contato">
                        <h4>Contatos e Informações</h4>
                        <ul class="colorlib-footer-links">
                            <li>Mumbaba de Baixo, Massapê-CE, <br> AV. Doca Pereira, Loja 01</li>
                            <li><a href="#">(88) 99288-9999</a></li>
                            <li><a href="#">a&tstore@a&t.com</a></li>
                            <li><a href="#">www.a&tstore.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copy" style="background-color: #ffffff">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>

                            <span class="block">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="icon-heart2"
                                    aria-hidden="true"></i> by <a href="https://colorlib.com"
                                    target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </span>
                            <span class="block">Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a>
                                , <a href="http://pexels.com/" target="_blank">Pexels.com</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
    <!-- Flexslider -->
    <script src="{{ asset('js/jquery.flexslider-min.js') }}"></script>
    <!-- Owl carousel -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- Magnific Popup -->
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/magnific-popup-options.js') }}"></script>
    <!-- Date Picker -->
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <!-- Stellar Parallax -->
    <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
    <!-- Main -->
    <script src="{{ asset('js/main.js') }}"></script>
	<script>
        $("#desconto").keyup(function() {
            let preco = parseFloat(document.getElementById("valorSemDesconto").value);
            let porcentagem = parseFloat(document.getElementById("desconto").value);
			var desconto = preco * (porcentagem / 100);
			var total = preco - desconto;
			if(Number.isNaN(total) || !isFinite(total))
			{
				document.getElementById("pedidoDesconto").innerHTML = preco.toFixed(2);
			}else{
				document.getElementById("pedidoDesconto").innerHTML = total.toFixed(2);
				document.getElementById("formDesconto").value = desconto.toFixed(2); 
			}
			
		});
	</script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
  @if(Session::has('message'))
  toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "4000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}; 
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
</body>

</html>