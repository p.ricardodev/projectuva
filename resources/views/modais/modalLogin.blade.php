<!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{ route('login') }}">
       @csrf
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">LOGIN
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
            </h4>
      </div>
      <div class="modal-body">
            <div class="row form-group">
                <div class="col-md-6 padding-bottom">
                    <label for="femail">E-mail</label>
                    <input id="femail" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Digite seu email" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror		
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-12">
                    <label for="email">Senha</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Digite sua senha" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                </div>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Entrar</button>
      </div>
     </form>
    </div>
  </div>
</div>