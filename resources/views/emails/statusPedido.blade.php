
<h3>Comprovante de compra</h3>
<h4>Obrigado pela preferência, aproveite a compra!</h4>

<a href="http://localhost/projectUVA/public/carrinho/compras">Veja suas Compras clicando aqui!</a>

<div style="width:300px; font-size:0.9rem;background-color:#ffffeb; padding:10px; font-family: Helvetica, sans-serif; margin: auto">
<h3>NOME DO CLIENTE: {{ $user->name }}</h3>
<h4>CPF: {{ $user->cpf }}</h4>
<h4>TELEFONE: {{ $user->telefone }}</h4>

<table style="text-align:center">
@forelse ($compras as $pedido)
                <h5 class="col l6 s12 m6"> Pedido: {{ $pedido->id }} </h5>
                <h5 class="col l6 s12 m6"> Criado em: {{ $pedido->created_at->format('d/m/Y H:i') }} </h5>

                <tr>
                    <th style="width:10px;">ITEM</th>
                    <th style="width:20px">COD</th>
                    <th>DESC</th>
                    <th>VALOR</th>
                </tr>
                        @php
                            $total_pedido = 0;
                            $item = 0;
                            $valor_desconto = 0;
                        @endphp
                        @foreach ($pedido->pedido_produtos_itens as $pedido_produto)
                            @php
                                $total_produto = $pedido_produto->valor;
                                $total_pedido += $total_produto;
                            @endphp

                            @forelse($pedido_produto->descontos as $desconto)
                             @php
                                $valor_desconto = $desconto->valor_desconto;
                                $totalFinalPedido = $total_pedido - $valor_desconto;
                            @endphp
                            @empty
                                @php
                                     $totalFinalPedido = $total_pedido;
                                @endphp
                            @endforelse

                            <tr> 
                                <td>{{ ($item =  $item + 1) }}</td>
                                <td>{{ $pedido_produto->produto->id }}</td>
                                <td>{{ $pedido_produto->produto->name }}
                               <td>R$ {{ number_format($total_produto, 2, ',', '.') }} </td>
                            </tr>
                        
                        @endforeach
                        <tr>
                            <th colspan="2">Total do pedido</th>
                            <th colspan="2">Total do Desconto</th>
                        </tr>

                        <tr>
                            <td colspan="2"> R$ {{ number_format($total_pedido, 2, ',', '.') }}</td>
                            <td colspan="2"> R$ @if($valor_desconto) {{ $valor_desconto }} @else 0 @endif </td>
                        </tr>
                        <tr>
                            <td colspan="4"><b>TOTAL </b> R$ {{ number_format($totalFinalPedido, 2, ',', '.') }}</td>
                        </tr>

                               
@empty
nenhum dado
@endforelse
</table>

<h3>A&T STORE LTDA</h3>
<h4>Contatos e Informações</h4>
    Mumbaba de Baixo, Massapê-CE,<br />
    AV. Doca Pereira, Loja 01 <br />
    (88) 99288-9999 <br />
    a&tstore@a&t.com <br />
    www.a&tstore.com <br />

</div>