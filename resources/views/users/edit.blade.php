@extends('layouts.layout')

@section('content')
    <div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">EDITAR USUÁRIO</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('user.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <form action="{{ route('user.update', ['user' => $user->id]) }}" method="post" class="mt-4" autocomplete="off">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="name">Nome do Usuário</label>
                                <input type="text" class="form-control" id="name" placeholder="Insira o nome completo"
                                       name="name" value="{{ old('name') ?? $user->name }}">
                            </div>
                            
                             <div class="form-group">
                                <label for="cpf">CPF</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="cpf"
                                       name="cpf" value="{{ old('cpf') ?? $user->cpf}}">
                                @if ($errors->has('cpf'))<span class="form-text help-block msg_validate">{{ $errors->first('cpf') }}</span>@endif
                            </div>
                            
                             <div class="form-group">
                                <label for="telefone">Telefone</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="telefone"
                                       name="telefone" value="{{ old('telefone') ?? $user->telefone }}">
                                @if ($errors->has('telefone'))<span class="form-text help-block msg_validate">{{ $errors->first('telefone') }}</span>@endif
                            </div>
                            
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" placeholder="Insira o e-mail do usuário"
                                       name="email" value="{{ old('email') ?? $user->email }}">
                            </div>
                            <div class="form-group">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" id="password" placeholder="digite uma nova senha"
                                       name="password" value="{{ old('password') }}">
                            </div>

                            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Alterar</button>
                            <a href="{{ route('user.index') }}" class="btn btn-info"><i class="fas fa-list"></i> Listar</a>
                            <a href="{{ route('/') }}" class="btn btn-danger"><i class="fas fa-ban"></i> Cancelar</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection
