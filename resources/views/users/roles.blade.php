@extends('layouts.layout')

@section('content')
    <div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">PERFIS</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('user.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <h2 class="mt-4">Perfis de: {{ $user->name }}</h2>

                        <form action="{{ route('user.roles', ['user' => $user->id]) }}" method="post" class="mt-4" autocomplete="off">
                            @csrf
                            @method('PUT')
                           @foreach($roles as $role)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="{{ $role->id }}" name="{{ $role->id }}" {{ ($role->can == '1' ? 'checked':'') }} />
                                    <label class="custom-control-label" for="{{ $role->id }}">{{ $role->name }}</label>
                                </div>
                          @endforeach
                            <BR /><button type="submit" class="btn  btn-success">Sincronizar Perfis</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection
