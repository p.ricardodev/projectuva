@extends('layouts.layout')

@section('content')
<div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">REGISTRO DE USUÁRIO</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('user.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />
                    <div class="card-body">

                        <form action="{{ route('user.store') }}" method="post" class="mt-4" autocomplete="off">
                            @csrf

                            <div class="form-group">
                                <label for="name">Nome do Usuário</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="name" autofocus placeholder="Digite seu nome"
                                       name="name" value="{{ old('name') }}">
                                @if ($errors->has('name'))<span class="form-text help-block msg_validate">{{ $errors->first('name') }}</span>@endif
                            </div>
                            
                             <div class="form-group">
                                <label for="cpf">CPF</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="cpf"
                                       name="cpf" value="{{ old('cpf') }}">
                                @if ($errors->has('cpf'))<span class="form-text help-block msg_validate">{{ $errors->first('cpf') }}</span>@endif
                            </div>
                            
                             <div class="form-group">
                                <label for="telefone">Telefone</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="telefone"
                                       name="telefone" value="{{ old('telefone') }}">
                                @if ($errors->has('telefone'))<span class="form-text help-block msg_validate">{{ $errors->first('telefone') }}</span>@endif
                            </div>
                            
                            <div class="form-group">
                                <label for="email">E-mail</label> <span class="obrigatorio">*</span>
                                <input type="email" class="form-control" id="email" placeholder="Digite um e-mail"
                                       name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))<span class="form-text help-block msg_validate">{{ $errors->first('email') }}</span>@endif

                            </div>
                            <div class="form-group">
                                <label for="password">Senha</label> <span class="obrigatorio">*</span>
                                <input type="password" class="form-control" id="password" placeholder="Digite uma senha"
                                       name="password" value="{{ old('password') }}">
                                @if ($errors->has('password'))<span class="form-text help-block msg_validate">{{ $errors->first('password') }}</span>@endif

                            </div>

                            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Cadastrar</button>
                            <a href="{{ route('user.index') }}" class="btn btn-info"><i class="fas fa-list"></i> Listar</a>
                            <a href="{{ route('/') }}" class="btn btn-danger"><i class="fas fa-ban"></i> Cancelar</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection
