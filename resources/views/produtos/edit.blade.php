@extends('layouts.layout')

@section('content')
<div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">EDITAR PRODUTOS</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('produto.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">
                        <form action="{{ route('produto.update', ['produto' => $produtos->id]) }}" method="post" class="mt-4" autocomplete="off">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="name">Nome do Produto</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="name" placeholder="Digite o nome do produto"
                                       name="name" value="{{ old('name') ?? $produtos->name }}">
                                @if ($errors->has('name'))<span class="form-text help-block msg_validate">{{ $errors->first('name') }}</span>@endif

                            </div>
                            
                            <div class="form-group">
                                <label for="descricao">Descrição</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="name" placeholder="Digite uma descrição do produto"
                                       name="descricao" value="{{ old('descricao') ?? $produtos->descricao }}">
                                @if ($errors->has('descricao'))<span class="form-text help-block msg_validate">{{ $errors->first('descricao') }}</span>@endif

                            </div>
                            
                            <div class="form-group">
                                <label for="preco_compra">Preço de Compra</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="preco_compra" placeholder="Digite o preço da Compra"
                                       name="preco_compra" value="{{ old('preco_compra') ?? $produtos->preco_compra }}">
                                @if ($errors->has('preco_compra'))<span class="form-text help-block msg_validate">{{ $errors->first('preco_compra') }}</span>@endif

                            </div>
                            
                            <div class="form-group">
                                <label for="preco_venda">Preço de Venda</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="name" placeholder="Digite o preço da venda"
                                       name="preco_venda" value="{{ old('preco_venda') ?? $produtos->preco_venda }}">
                                @if ($errors->has('preco_venda'))<span class="form-text help-block msg_validate">{{ $errors->first('preco_venda') }}</span>@endif

                            </div>
                            
                            <div class="form-group">
                                <label for="categoria_id">Categoria</label> <span class="obrigatorio">*</span>
                                <select name="categoria_id" class="form-control">
                                    <option value="">Selecione um destaque para produtos</option>
                                    @foreach($categorias as $categoria)
                                    <option value="{{ $categoria->id  }}" {{ ($produtos->categoria_id == $categoria->id) ? 'selected':''  }}>{{ $categoria->name }}</option>
                                    @endforeach
                                </select>       

                                @if ($errors->has('categoria_id'))<span class="form-text help-block msg_validate">{{ $errors->first('categoria_id') }}</span>@endif

                            </div>    
                            
                            <div class="form-group">
                                <label for="qtd_estoque">Qtd. no Estoque</label> <span class="obrigatorio">*</span>
                                <input type="text" class="form-control" id="name" placeholder="Digite uma permissão"
                                       name="qtd_estoque" value="{{ old('qtd_estoque') ?? $produtos->qtd_estoque}}">
                                @if ($errors->has('qtd_estoque'))<span class="form-text help-block msg_validate">{{ $errors->first('qtd_estoque') }}</span>@endif

                            </div>
                        
                            <div class="form-group">
                                <label for="status">Visibilidade do Produto</label>
                                <select name="status" class="form-control">
                                    <option value="">Selecione um destaque para produtos</option>
                                    <option value="Lançamento" @if($produtos->status === 'Lançamento')selected @endif>Lançamento</option>
                                    <option value="Importado" @if($produtos->status === 'Importado')selected @endif>Importado</option>
                                </select>                               
                            </div>
                            
                            <button type="submit" class="btn btn-success">Alterar</button>
                             <a href="{{ route('produto.index') }}" class="btn btn-info"><i class="fas fa-list"></i> Listar</a>
                            <a href="{{ route('/') }}" class="btn btn-danger"><i class="fas fa-ban"></i> Cancelar</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection