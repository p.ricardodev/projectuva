@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">LISTAGEM DE PRODUTOS</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('produto.create') }}">&plus; Novo</a></div>
                        </div>
                    </div><br />
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <table class="table table-striped mt-4">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Produto</th>
                                <th>Descrição</th>
                                <th>Preço de Compra</th>
                                <th>Preço de venda</th>
                                 <th>Qtd. Estoque</th>
                                 <th>Imagem</th>
                                <th class="th-center">Editar</th>
                                <th class="th-center">Excluir</th>
                            </tr>
                            </thead>
                            <tbody>

                           @forelse($produtos as $produto)
                           <tr>
                                    <td>{{ $produto->id }}</td>
                                    <td>{{ $produto->name }}</td>
                                    <td>{{ $produto->descricao }}</td> 
                                    <td>{{ $produto->preco_compra }}</td>
                                     <td>{{ $produto->preco_venda }}</td> 
                                    <td>{{ $produto->qtd_estoque }}</td>
                                    <td><img src="{{ asset('images/'.$produto->imagem) }}" style="width:50px" /></td>
                                    
                                    <td class="th-center"><a class="btn btn-link" href="{{ route('produto.edit', ['produto' => $produto->id] ) }}">Editar</a></td>
                                    <td class="th-center">
                                        <form action="{{ route('produto.destroy', ['produto' => $produto->id]) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <input class="btn btn-link" type="submit" value="Remover">
                                        </form>
                                    </td>
                                </tr>
                                @empty
                            <p>Nenhum produto cadastrado!</p>
                          @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><br /><br />
@endsection
