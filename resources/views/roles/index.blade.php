@extends('layouts.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                   <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">LISTAGEM DOS PERFIS</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('role.create') }}">&plus; Novo</a></div>
                        </div>
                    </div><br />

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <table class="table table-striped mt-4">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Perfis</th>
                                <th class="th-center">Editar</th>
                                <th class="th-center">Permissões</th>
                                <th class="th-center">Deletar</th>
                            </tr>
                            </thead>
                            <tbody>

                           @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td class="th-center"><a class="btn btn-link" href="{{ route('role.edit', ['role' => $role->id] ) }}">Editar</a></td>
                                       <td class="th-center">  <a class="btn btn-link" href="{{ route('role.permissions', ['role' => $role->id]) }}">Permissões</a></td>
                                       <td class="th-center">  <form action="{{ route('role.destroy', ['role' => $role->id]) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <input class="btn btn-link" type="submit" value="Remover">
                                        </form>
                                    </td>
                                </tr>
                          @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><br />
@endsection
