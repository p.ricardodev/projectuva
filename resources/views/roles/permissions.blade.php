@extends('layouts.layout')

@section('content')
    <div class="container" style="background-color: #e9e9e9; padding: 2%">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                     <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">CADASTRAR PERMISSÕES</div>
                            <div class="col-md-2"><a class="text-success" href="{{ route('role.index') }}">&leftarrow; Voltar para a listagem</a></div>
                        </div>
                    </div><br />
                    <div class="card-body">

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger mt-4" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <h2 class="mt-4">Permissões para: {{ $role->name }}</h2>

                        <form action="{{ route('role.permissions', ['role' => $role->id]) }}" method="post" class="mt-4" autocomplete="off">
                            @csrf
                            @method('PUT')
                           @foreach($permissions as $permission)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="{{ $permission->id }}" name="{{ $permission->id }}" {{ ($permission->can == '1' ? 'checked':'') }} />
                                    <label class="custom-control-label" for="{{ $permission->id }}">{{ $permission->name }}</label>
                                </div>
                          @endforeach
                            <button type="submit" class="btn btn-success">Sincronizar Permissões</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div><br />
@endsection
