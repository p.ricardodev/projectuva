<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Pedido;
use App\Desconto;

class StatusPedido extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $compras;
    private $user;

    public function __construct($compras, $user)
    {
        $this->compras = $compras;
        $this->user = $user;
    }

    public function build()
    {
       
        return $this
        ->to($this->user->email)
        ->subject("Dados do Pedido")
        ->view('emails.statusPedido')->with(['compras' => $this->compras, 'user' => $this->user]);
    }
}
