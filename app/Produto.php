<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['name', 'descricao', 'preco_compra', 'preco_venda', 'categoria_id', 'qtd_estoque', 'imagem', 'status', 'created_at', 'updated_at'];
}
