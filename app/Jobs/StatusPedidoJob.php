<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Pedido;

class StatusPedidoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 5;
    private $compras;
    private $user;

    public function __construct($compras, $user)
    {
        $this->compras = $compras;
        $this->user = $user;
    }

    public function handle()
    {
        \Illuminate\Support\Facades\Mail::send(New \App\Mail\StatusPedido($this->compras, $this->user));
    }
}
