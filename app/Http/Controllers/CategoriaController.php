<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    private $categorias;
    
    public function __construct() {
        $categorias = $this->categorias;
    }
    
    public function index()
    {
        $categorias = Categoria::all();
        return view('categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('categorias.create');
    }
    
    private function validaForm($request) {
         $validatedData = $request->validate
        ([
            'name' => 'required|max:150'
        ]);
    }

    public function store(Request $request)
  {
      $this->validaForm($request);
      
      $categorias = new Categoria;
      $categorias->name = $request->name;

      $warning ='Registro já cadastrado!';
      $success ='Registro cadastrado com sucesso!';
      return $this->inserting($categorias, $warning, $success);
  }

  public function edit($id)
  {
      $categorias = Categoria::find($id);
      return view('categorias.edit', compact('categorias'));
  }

  function update(Request $request, $id)
  {
      $this->validaForm($request);
     
      $categorias = Categoria::find($id);
      $categorias->name = $request->name;
     
      $warning = 'Registro já existe com esses dados!';
      $success = 'Registro editado com sucesso!';
      return $this->updating($categorias,$warning,$success);

  }

  private function inserting($categorias, $warning, $success)
  {
    $checking = $categorias->where(['name'=> $categorias->name])->exists();

    if($checking === true) {
        $notifications = array('message' => 'Erro ao efetuar o Cadastro!', 'alert-type' => 'danger');
        return back()->with($notifications);
    }else{
        $categorias->save();
        $notifications = array('message' => 'Cadastro efetuado com sucesso!', 'alert-type' => 'success');
        return back()->with($notifications);
    }
  }

  private function updating($categorias, $warning, $success)
  {
    $checking = $categorias->where(['name'=> $categorias->name])->exists();
    if($checking === true) {
        $notifications = array('message' => 'Erro ao atualizar!', 'alert-type' => 'danger');
        return back()->with($notifications);
    }else{
        $categorias->update();
        $notifications = array('message' => 'Cadastro alterado com sucesso!', 'alert-type' => 'success');
        return back()->with($notifications);
    }
  }

  public function destroy($id)
  {
      $categorias = Categoria::find($id);
      $checking = $categorias->where('id',$id)->exists();

      if($categorias->delete()) {
        $notifications = array('message' => 'Cadastro deletado com sucesso!', 'alert-type' => 'success');
        return redirect()->route('categoria.index')->with($notifications);
        }else{
        $notifications = array('message' => 'Erro ao deletar o Cadastro!', 'alert-type' => 'danger');
        return redirect()->route('categoria.index')->with($notifications);
    }

  }
}
