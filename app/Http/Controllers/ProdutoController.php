<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Categoria;
use File;

class ProdutoController extends Controller
{
    private $produtos;

    public function __construct() {
        $produtos = $this->produtos;
    }

    public function index()
    {
        $produtos = Produto::all();
        return view('produtos.index', compact('produtos'));
    }

    public function create()
    {
        $categorias = Categoria::all();
        return view('produtos.create', compact('categorias'));
    }
    
    private function validaForm($request) {
         $validatedData = $request->validate
        ([
            'name' => 'required|max:150',
            'descricao' => 'required|max:150',
            'preco_compra' => 'required',
            'preco_venda' => 'required',
            'categoria_id' => 'required|integer',
            'qtd_estoque' => 'required|integer',
            'status' => 'max:150'
            
        ]);
    }

    public function store(Request $request)
  {
      $this->validaForm($request);
      
      $produtos = new Produto;
      $produtos->name = $request->name;
      $produtos->descricao = $request->descricao;
      $produtos->preco_compra = $request->preco_compra;
      $produtos->preco_venda = $request->preco_venda;
      $produtos->categoria_id = $request->categoria_id;
      $produtos->qtd_estoque = $request->qtd_estoque;
      $produtos->status = $request->status;

      if ($request->imagem !== null) {
        $nomeimagem = time().'.'.$request->imagem->extension();  
        $request->imagem->move(public_path('images'), $nomeimagem);
        $produtos->imagem = $nomeimagem;
    }else{
        $produtos->imagem = "avatar.png";    
    } 

      $warning ='Registro já cadastrado!';
      $success ='Registro cadastrado com sucesso!';
      return $this->inserting($produtos, $warning, $success);
  }

  public function edit($id)
  {
      $produtos = Produto::find($id);
      $categorias = Categoria::all();
      return view('produtos.edit', compact('produtos','categorias'));
  }

  function update(Request $request, $id)
  {
      $this->validaForm($request);
     
      $produtos = Produto::find($id);
      $produtos->name = $request->name;
      $produtos->descricao = $request->descricao;
      $produtos->preco_compra = $request->preco_compra;
      $produtos->preco_venda = $request->preco_venda;
      $produtos->categoria_id = $request->categoria_id;
      $produtos->qtd_estoque = $request->qtd_estoque;
      $produtos->status = $request->status;
      
      return $this->updating($produtos);

  }

  private function inserting($produtos)
  {
    $checking = $produtos->where(['name'=> $produtos->name])->exists();
    if($checking === true) {
        $notifications = array('message' => 'Erro ao efetuar o Cadastro!', 'alert-type' => 'danger');
        return back()->with($notifications);
    }else{
        $produtos->save();
        $notifications = array('message' => 'Cadastro efetuado com sucesso!', 'alert-type' => 'success');
        return back()->with($notifications);
    }
  }

  private function updating($produtos)
  {
    $checking = $produtos->where(['name'=> $produtos->name ])->exists();
    if($checking === true) {
        $notifications = array('message' => 'Erro ao atualizar!', 'alert-type' => 'danger');
        return back()->with($notifications);
    }else{
        $produtos->update();
        $notifications = array('message' => 'Cadastro alterado com sucesso!', 'alert-type' => 'success');
        return back()->with($notifications);
    }

  }

  public function destroy($id)
  {
    $produtos = Produto::where('id', $id)->get()->first();
        File::delete("images/{$produtos->imagem}");


      $checking = $produtos->where('id',$id)->exists();

      if($produtos->delete()) {
        $notifications = array('message' => 'Cadastro deletado com sucesso!', 'alert-type' => 'success');
        return redirect()->route('produto.index')->with($notifications);
        }else{
        $notifications = array('message' => 'Erro ao deletar o Cadastro!', 'alert-type' => 'danger');
        return redirect()->route('produto.index')->with($notifications);
    }
  }

}
