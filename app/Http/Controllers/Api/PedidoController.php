<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pedido;
use App\Desconto;
use App\PedidoProduto;
use App\Produto;
use DB;

class PedidoController extends Controller
{
    public function apiPedidos(Request $request)
    {        
        return $compras = DB::table('pedido_produtos')
        ->join('pedidos', 'pedidos.id', '=', 'pedido_produtos.pedido_id')
        ->join('users', 'users.id', '=', 'pedidos.user_id')
        ->join('descontos', 'descontos.pedido_id', '=', 'pedido_produtos.pedido_id')
        
        ->select('users.name as users','pedidos.created_at as dataPedido', 'pedido_produtos.pedido_id as pedido', 'descontos.valor_desconto', 
        DB::raw('SUM(pedido_produtos.valor) as totalValor'), 
        DB::raw('(SUM(pedido_produtos.valor) - descontos.valor_desconto) as totalCompra'), )

        ->groupBy('pedido_produtos.pedido_id', 'pedidos.created_at', 'users.name', 'descontos.valor_desconto')
        ->orderBY('pedidos.created_at', 'desc')
        ->get();        

    }

}
