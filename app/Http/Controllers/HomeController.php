<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\PedidoProduto;

class HomeController extends Controller
{
 
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        $lancamentos = Produto::where('status', '=', 'Lançamento')->orderBy('id', 'desc')->limit(4)->get();
        $importados = Produto::where('status', '=', 'Importado')->orderBy('id', 'desc')->limit(4)->get();
        $produtos = Produto::where('status', '=', null)->orderBy('id', 'desc')->paginate(12);
        $qtdpedidos = PedidoProduto::where('status', '=', 'RE')->count();

        return view('home', compact('produtos', 'lancamentos', 'importados', 'qtdpedidos'));
    }
    
    public function shop()
    {
        return view('shop.shop');
    }
}
