<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desconto extends Model
{
    protected $fillable = [
        'pedido_id',
        'valor_desconto',
        'created_at',
        'updated_at'
    ];

    public function pedido_produtos_itens()
    {
        return $this->hasMany('App\PedidoProduto');
    }

}
