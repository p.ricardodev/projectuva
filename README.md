## Descrição do Projeto

Projeto de gestão e controle de pedidos web;

Login com ACL usando o Spatie;

Os pedidos podem ser cancelados até 1 dia util da confirmação;

Área de carrinho dos pedidos e minhas compras.

Cadastros:

<ul>

<li>Usuários</li>

<li>Clientes</li>

<li>Categorias</li>

<li>Produtos</li>

</ul>

Envia para usuário que finalizou a compra um e-mail com link para acessar as informações do pedido;

A consulta das vendas é consumida via api com <b> Vue.js 2x </b> <a  href="https://br.vuejs.org/v2/guide/installation.html"><img  src="https://img.shields.io/static/v1?label=vue.js 2x&message=framework&color=white&style=for-the-badge&logo=vue.js"/></a>

## Tecnologias

Laravel 6.18.3 <a  href="https://laravel.com/docs/6.x/installation"><img  src="https://img.shields.io/static/v1?label=Laravel&message=framework&color=orange&style=for-the-badge&logo=LARAVEL"/></a><br />

Spatie 3.15 <a  href="https://spatie.be/docs/laravel-permission/v3/installation-laravel"><img  src="https://img.shields.io/static/v1?label=Spatie&message=package&color=orange&style=for-the-badge&logo=LARAVEL"/></a><br />

Bootstrap 4 <a  href="https://getbootstrap.com/"><img  src="https://img.shields.io/static/v1?label=Bootstrap&message=framework&color=orange&style=for-the-badge&logo=BOOTSTRAP"/></a><br />

Template for <a  href="https://colorlib.com/">Colorlib</a>

## Instalação

composer install (Instalação do diretório <b>/vendor</b> e dependências do projeto) <br />

npm install <br />

Criar arquivo .env (Copiar do .env.example) <br />

Crie a base de dados com nome <b>ecommerce</b> <br />

No diretório do projeto execute os comandos:

Criar APP_KEY
<code>
php artisan key:generate
</code>

Criar as tabelas do banco de dados com os <b>migrates</b> e popular de dados com <b>seed</b>

<code>
php artisan migrate --seed
</code>

Iniciar o projeto
<code>
php artisan serve
</code>

O envio de E-mail foi configurado usando o smtp do gmail, as filas já estão parametrizadas (queue/job), então o comando queue:work deve ser inicializado no terminal.
<code>
php artisan queue:work
</code>

## Testes

Foi imlementado testes de Feature e Unitários, utilize o comando:
<code>
vendor\bin\phpunit
</code>
