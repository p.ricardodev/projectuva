<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\categoria;
class CategoriaTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCheckColumnsIsCorrect()
    {
        $category = new Categoria;

        $expected = [
            'name',
            'created_at',
            'updated_at'
        ];

        $arrayCompared = array_diff($expected, $category->getFillable());

        $this->assertEquals(0, count($arrayCompared));
    }
}
